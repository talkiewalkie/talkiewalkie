class AddAdditionalParticipationsNumberToParticipations < ActiveRecord::Migration[7.0]
  def change
    add_column :participations, :additional_participations_number, :integer, default: 0
  end
end
