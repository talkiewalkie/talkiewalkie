class AddNotNullOnAdditionalParticipationsNUmber < ActiveRecord::Migration[7.0]
  def change
    change_column_null :participations, :additional_participations_number, false
  end
end
