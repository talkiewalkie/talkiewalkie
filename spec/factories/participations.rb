FactoryBot.define do
  factory :participation do
    user { create(:user) }
    event { create(:event) }
  end
end
