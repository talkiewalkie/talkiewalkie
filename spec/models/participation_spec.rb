require 'rails_helper'

RSpec.describe Participation, type: :model do
  describe "::new" do

    subject { build(:participation) }

    context "with valid attributes" do
      it { is_expected.to be_valid }
    end

    context "with invalid attributes" do
      it "return an error without a user" do
        subject.user = nil
        subject.valid?
        expect(subject.errors).to include(:user)
      end

      it "return an error without an event" do
        subject.event = nil
        subject.valid?
        expect(subject.errors).to include(:event)
      end
    end

  end
end
