require 'rails_helper'

RSpec.describe User, type: :model do
  describe "::new" do

    subject { build(:user) }

    context "with valid attributes" do
      it { is_expected.to be_valid }
    end

    context "with invalid attributes" do
      it "return an error without a password" do
        subject.password = nil
        subject.valid?
        expect(subject.errors).to include(:password)
      end

      it "return an error without a firstname" do
        subject.firstname = nil
        subject.valid?
        expect(subject.errors).to include(:firstname)
      end
    end

  end

end
