require 'rails_helper'

RSpec.describe Event, type: :model do
  describe "::new" do

    subject { build(:event) }

    context "with valid attributes" do
      it { is_expected.to be_valid }
    end

    context "with invalid attributes" do
      it "return an error without an author" do
        subject.author = nil
        subject.valid?
        expect(subject.errors).to include(:author)
      end

      it "return an error without a title" do
        subject.title = nil
        subject.valid?
        expect(subject.errors).to include(:title)
      end
    end

  end
end
