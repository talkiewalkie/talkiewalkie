# Rails 7 Hotwire (Turbo + Stimulus) WebSocket application
Reactive single-page application thanks to turbo-rails library, using websocket to broadcast updates to all users.

## App running here :
https://talkiewalkie-tribe-staging.herokuapp.com/

tom@gmail.com
huck@gmail.com
becky@gmail.com

password: aaaaaa

## Run locally :

* rails ./bin/setup
* rails server

=> app running on http://localhost:3000

## Following Turbo Rails tutorial authored by Alexandre Ruban
https://www.hotrails.dev/turbo-rails
