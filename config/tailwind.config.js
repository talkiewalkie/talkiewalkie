const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [
    "./app/helpers/**/*.rb",
    "./app/javascript/**/*.js",
    "./app/views/**/*",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter var", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        black: "#00080f",
        blue: {
          200: "#d4dee8",
          400: "#9ED3FF",
          600: "#38709b",
          800: "#83a1b7",
        },
        gray: {
          200: "#e8e7e4",
        },
        green: {
          300: "#bad2af",
        },
        orange: {
          100: "#d5d1c7",
          200: "#E3C68C",
          300: "#e1d9c7",
          500: "#FF9C00",
          700: "#bfb294",
        },
      },
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/aspect-ratio"),
    require("@tailwindcss/typography"),
  ],
};
