Rails.application.routes.draw do
  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root to: 'pages#home'

  scope '(:locale)', locale: /fr|en/ do
    get '/:locale' => 'events#index'
    root to: 'events#index'

    resources :events do
      # delete :destroy_current_user_participation, on: :member
      resources :participations, only: [:create]
      resource :participations, only: [] do
        delete :destroy_current_user_participation, on: :member
      end
    end
  end
end
