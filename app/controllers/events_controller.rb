class EventsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]

  before_action :set_event, only: [:edit]
  before_action :set_event_with_loading_participations, only: [:update, :show, :destroy]   # TODO : remove destroy and use set_event when just updating event with canceled: true

  def index
    @events = policy_scope(Event).includes(:author, participations: :user).order(created_at: :desc)

    @event= Event.new
  end

  def show
  end

  def new
    @event = current_user.sent_events.build
    authorize @event
  end

  def create
    @event = current_user.sent_events.build(event_params)
    authorize @event

    respond_to do |format|
      if @event.save
        # format.turbo_stream # { render turbo_stream: turbo_stream.prepend(:events, partial: "events/event", locals: {event: @event})}
        format.html { redirect_to events_path }
      else
        format.turbo_stream { render turbo_stream: turbo_stream.replace(@event, partial: "events/form", locals: {event: @event})}
        format.html { render :new }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @event.update(event_params)
        format.turbo_stream { render turbo_stream: turbo_stream.replace(@event, partial: "events/show_event", locals: { event: @event, participations_count: nil }) }
        format.html { redirect_to @event }
      else
        format.turbo_stream { render turbo_stream: turbo_stream.replace(@event, partial: "events/form", locals: {event: @event})}
        format.html { render :edit }
      end
    end
  end

  def destroy
    @event.destroy

    redirect_to events_path
  end

  private

  def set_event
    @event = Event.includes(:author).find(params[:id])
    authorize @event
  end

  def set_event_with_loading_participations
    @event = Event.includes(:author, participations: :user).find(params[:id])
    authorize @event
  end

  def event_params
    params.require(:event).permit(:author, :title, :description)
  end
end