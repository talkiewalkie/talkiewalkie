class ApplicationController < ActionController::Base

  include Pundit

  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  after_action :verify_authorized, except: :index, unless: :devise_controller?
  after_action :verify_policy_scoped, only: :index, unless: :devise_controller?

  # before_action :set_locale
  around_action :switch_locale

  def switch_locale(&action)
    locale = params[:locale] || I18n.default_locale
    I18n.with_locale(locale, &action)
  end

  def default_url_options
    { locale: I18n.locale == I18n.default_locale ? nil : I18n.locale }
  end

  # def set_locale
  #   I18n.locale = params.fetch(:locale, I18n.default_locale).to_sym
  # end

  # def switch_locale(&action)
  #   logger.debug "* Accept-Language: #{request.env['HTTP_ACCEPT_LANGUAGE']}"
  #   locale = extract_locale_from_accept_language_header
  #   logger.debug "* Locale set to '#{locale}'"
  #   I18n.with_locale(locale, &action)
  # end

  # rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # def user_not_authorized
  #   flash[:alert] = t 'user_not_authorized'
  #   redirect_to(root_path)
  # end

  # def http_basic_authenticate
  #   authenticate_or_request_with_http_basic do |name, password|
  #     name == 'whatever' && password == 'strong_password'
  #   end
  # end

  # if Rails.env.staging?
  #   before_action :http_basic_authenticate
  # end

  private

  def extract_locale_from_accept_language_header
    request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:firstname, :lastname])
    devise_parameter_sanitizer.permit(:account_update, keys: [:firstname, :lastname])
  end
end
