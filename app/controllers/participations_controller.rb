class ParticipationsController < ApplicationController
  before_action :set_event
  # before_action :set_participation, only: [:destroy_participation]

  def create
    @participation = @event.participations.build(participation_params)
    @participation.user = current_user
    authorize @participation

    respond_to do |format|
      if @participation.save
        Turbo::StreamsChannel.broadcast_update_to "events", target: "event_#{@event.id}_participations_count", html: "#{@participations_count + 1 + @participation.additional_participations_number}"
        Turbo::StreamsChannel.broadcast_update_to @event, target: "event_#{@event.id}_participations_count", html: "#{@participations_count + 1 + @participation.additional_participations_number}"
        format.turbo_stream { render turbo_stream: turbo_stream.replace("event_#{@event.id}_links", partial: "events/links/cancel_decline", locals: { event: @event }) }
        # { render turbo_stream: turbo_stream.replace(@event, partial: "events/event", locals: { event: @event }) }
        format.html { redirect_to events_path }
      else
        # TODO : display flash alert
        format.turbo_stream { render turbo_stream: turbo_stream.replace("event_#{@event.id}_links", partial: "events/links/accept_decline", locals: { event: @event, participation: @participation }) }
        format.html { redirect_to events_path }
      end
    end
  end

  def destroy_current_user_participation
    @current_user_participation = @event_participations.find_by(user: current_user)
    authorize @current_user_participation

    additional_participations_number = @current_user_participation.additional_participations_number

    @current_user_participation.destroy

    Turbo::StreamsChannel.broadcast_update_to "events", target: "event_#{@event.id}_participations_count", html: "#{@participations_count - 1 - additional_participations_number}"
    Turbo::StreamsChannel.broadcast_update_to @event, target: "event_#{@event.id}_participations_count", html: "#{@participations_count - 1 - additional_participations_number}"
    respond_to do |format|
      format.turbo_stream { render turbo_stream: turbo_stream.replace("event_#{@event.id}_links", partial: "events/links/accept_decline", locals: { event: @event, participation: @event.participations.new }) }
      format.html { redirect_to events_path }
    end
  end

  private

  def set_event
    @event_participations = Participation.includes(:event, :user).where(event_id: params[:event_id])
    @participations_count = @event_participations.size
    if @participations_count > 0
      @event = @event_participations.first.event
      @participations_count += @event.participations.sum(&:additional_participations_number)
    else
      @event = Event.find(params[:event_id])
    end

    # @event = Event.eager_load(:participations).find(params[:event_id])
    # @participations_count = @event.participations.size

    # @event = Event.left_outer_joins(:participations).distinct
    #               .select("events.*, COUNT(participations.*) AS participations_count").group("events.id")
    #               .find(params[:event_id])

  end

  # def set_participation
  #   @participation = Participation.find_by(event_id: params[:event_id], user: current_user)
  # end

  def participation_params
    params.require(:participation).permit(:additional_participations_number)
  end

end