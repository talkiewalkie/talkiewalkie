module EventHelper 
  def event_status(event)
    if policy(event).destroy?
      "owned"
    elsif policy(event).destroy_current_user_participation?
      "accepted"
    else
      "waiting"
    end
  end
end