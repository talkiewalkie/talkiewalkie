class Participation < ApplicationRecord
  belongs_to :user
  belongs_to :event

  after_create_commit :append_participation #, :update_participations_count
  after_destroy_commit :remove_participation #, :update_participations_count

  validates :additional_participations_number, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  private

  def append_participation
    broadcast_append_to("events", target: "event_#{event.id}_participations")
    broadcast_append_to(event, target: "event_#{event.id}_participations")
    # broadcast_update_to("events", target: "event_#{event.id}_participations_count", html: "#{event.participations_count + 1}")
    # broadcast_update_to("events", target: "event_#{event.id}_participations_count", html: "#{@participations_count + 1}")
  end

  def remove_participation
    broadcast_remove_to "events"
    broadcast_remove_to event
    # broadcast_update_to("events", target: "event_#{event.id}_participations_count", html: "#{event.participations_count - 1}")
  end

  # def update_participations_count
  #   broadcast_update_to(event, target: "event_#{event.id}_participations_count", html: "Participants : #{event.participations.size}")
  # end

end
