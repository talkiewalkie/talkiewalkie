class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  has_many :sent_events, class_name: "Event", foreign_key: "author_id", inverse_of: "author"
  has_many :participations
  
  validates :firstname, presence: true

  def name
    lastname ? "#{firstname} #{lastname}" : firstname
  end

end
