class Event < ApplicationRecord
  belongs_to :author, class_name: "User", inverse_of: "sent_events"
  has_many :participations, dependent: :destroy

  validates :title, presence: true

  after_create_commit :append_event
  after_update_commit :replace_event
  after_destroy_commit { broadcast_remove_to "events" }

  private

  def append_event
    broadcast_prepend_to "events", partial: "events/append_event", locals: { event: self, participations_count: 0 }
  end

  def replace_event
    broadcast_replace_to "events", target: "core_event_#{self.id}", partial: "events/event_core", locals: { event: self, participations_count: nil }
    broadcast_replace_to self, target: "core_event_#{self.id}", partial: "events/event_core", locals: { event: self, participations_count: nil }
  end

end
