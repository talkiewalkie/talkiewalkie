import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static values = {
    status: String,
  };

  static targets = ["accepted", "waiting"];

  acceptedTargetConnected() {
    this.statusValue = "accepted";
  }

  waitingTargetConnected() {
    this.statusValue = "waiting";
  }
}
