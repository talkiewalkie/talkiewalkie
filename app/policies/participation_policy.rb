class ParticipationPolicy < ApplicationPolicy
  
  def show?
    true
  end

  def create?
    true
  end

  def update?
    user_is_owner?
  end

  def destroy?
    user_is_owner?
  end

  def destroy_current_user_participation?
    user_is_owner?
  end

  private

  def user_is_owner?
    record.user == user
  end

end