class EventPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      # TODO : restrict to event receivers that have author in theirs contacts
      # scope.joins(:event_receivers).distinct.where(event_receivers: { user: user }).where(author: [user.contacts])
      scope.all
    end
  end

  def show?
      # TODO : restrict to event receivers that have author in theirs contacts
      # scope.joins(:event_receivers).distinct.where(event_receivers: { user: user }).where(author: [user.contacts])
    true
  end

  def create?
    true
  end

  def edit?
    user_is_owner?
  end

  def update?
    user_is_owner?
  end

  def destroy?
    user_is_owner?
  end

  def destroy_current_user_participation?
    user && record.participations.pluck(:user_id).include?(user.id)
  end

  private

  def user_is_owner?
    record.author == user
  end

end
